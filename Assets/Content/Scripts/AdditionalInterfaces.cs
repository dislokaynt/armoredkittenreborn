﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IParrentable<T, T1>
{
    T FirstParent { get; }
    T1 SecondParent { get; }
}
public interface IInitable
{
    void Init();
}
