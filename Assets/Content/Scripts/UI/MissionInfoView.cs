﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public class MissionInfoView : MonoBehaviour
{
    public Text Name;
    public Text Progress;
    public Text ProgressStatus;
    public RectTransform IconParent;
    public Image Icon;

    public void Init(Mission mission)
    {
        IconParent.SetActiveSafe(mission.HasIcon);
        ProgressStatus.text = mission.CurrentCount + " / " + mission.NeedTargetCount;
        //TODO: ConnectionCollector
        var cashedCount = 0;
        mission.CurrentValueChanged.Listen(() =>
        {
            var currentCount = mission.CurrentCount;
            if (currentCount == cashedCount)
                return;
            ProgressStatus.text = currentCount + " / " + mission.NeedTargetCount;
            cashedCount = currentCount;
        });
    }
}