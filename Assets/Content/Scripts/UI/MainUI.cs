﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainUI : StandartLinksBehaviour
{
    public HealthView HealthView;
    public MissionInfoView MissionView;
    public void InitView()
    {
        HealthView.InitHpListener(InstanceController.PlayerTransform.GetComponent<HpComponent>());
        MissionView.Init(InstanceController.CurrentMission);
    }
}
