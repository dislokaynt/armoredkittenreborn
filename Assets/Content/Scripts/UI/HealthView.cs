﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class HealthView : StandartLinksBehaviour
{
    public Image HealthImageStatus;
    public Text HpCountText;

    public HpComponent HpTarget;
    public void InitHpListener(HpComponent target)
    {
        HpTarget = target;
        //TODO: ConnectionCollector
        HpTarget.ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(message =>
        {
            HealthImageStatus.fillAmount = HpTarget.CurrentHp / (float) HpTarget.MaxHp;
            HpCountText.text = HpTarget.CurrentHp.ToString();
        });
    }
}
