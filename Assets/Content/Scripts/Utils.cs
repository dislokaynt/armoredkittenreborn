﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Utils
{
    public static T GetRandomElement<T>(this IEnumerable<T> array)
    {
        var count = array.Count();
        var index = UnityEngine.Random.Range(0, count);
        if (index >= count)
            return default(T);
        return array.ElementAt(index);
    }
    /// <summary>
    /// Get random element from IEnumerable by weight
    /// Weight must be bigger, than 0
    /// </summary>
    public static T GetWeightedRandomElement<T>(this IEnumerable<(float, T)> array)
    {
        var sum = array.Sum(pair => pair.Item1 > 0 ? pair.Item1 : 0);
        var index = UnityEngine.Random.Range(0, sum);
        return array.First(pair=>
        {
            index -= pair.Item1;
            return index <= 0;
        }).Item2;
    }

    public static T GetWeightedRandomElement<T>(this IEnumerable<T> array, Func<T, float> weightFunc)
    {
        var sum = array.Sum(element => weightFunc(element) > 0 ? weightFunc(element) : 0);
        var index = UnityEngine.Random.Range(0, sum);
        return array.First(element =>
        {
            index -= weightFunc(element);
            return index <= 0;
        });
    }

    public static float GetAngle(Vector2 position)
    {
        var normalizedVector = position.normalized;
        return Mathf.Atan2(normalizedVector.y, normalizedVector.x);
    }
    public static Vector2 GetAngle(float degree)
    {
        degree = degree * Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(degree), Mathf.Sin(degree)).normalized;
    }

    public static void SetActiveSafe(this GameObject obj, bool state)
    {
        if (obj != null && obj.activeSelf != state)
            obj.SetActive(state);
    }
    
    public static void SetActiveSafe(this Component obj, bool state)
    {
        if (obj != null && obj.gameObject.activeSelf != state)
            obj.gameObject.SetActive(state);
    }
    
    public static RectTransform GetRectTransform(this GameObject obj)
    {
        return (RectTransform)obj.transform;
    }
    public static RectTransform GetRectTransform(this Component obj)
    {
        return (RectTransform)obj.transform;
    }

    public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
    {
        foreach (var element in list)
        {
            action(element);
        }
    }
}

public class Pool<T> : IDisposable where T : PoolObject
{
    private ConnectionCollector _collector = new ConnectionCollector();
    private List<T> _poolObjects;
    private Transform _poolParent;
    private Func<T> _objectsFabric;
    private List<T> _activeObjects;

    public List<T> ActiveObjects => _activeObjects;

    public Pool(Transform poolParent, Func<T> objectsFabric, int startObjectsCount)
    {
        _poolObjects = new List<T>();
        _activeObjects = new List<T>();
        _poolParent = poolParent;
        _objectsFabric = objectsFabric;
        for (int i = 0; i < startObjectsCount; i++)
        {
            InstantiateAdditionalPrefab();
        }
    }

    private T InstantiateAdditionalPrefab()
    {
        var poolObject = _objectsFabric.Invoke();
        poolObject.transform.SetParent(_poolParent);
        _poolObjects.Add(poolObject);
        return poolObject;
    }

    public T GetPoolObject(Action<T> initializeAction = null)
    {
        if (_poolObjects.Count == 0)
            InstantiateAdditionalPrefab();
        var poolObject = _poolObjects[0];
        _poolObjects.RemoveAt(0);
        _collector.Add = poolObject.ReturnToPoolStream.Listen(() =>
        {
            poolObject.SetActiveSafe(false);
            _poolObjects.Add(poolObject);
            _activeObjects.Remove(poolObject);
        });
        initializeAction?.Invoke(poolObject);
        _activeObjects.Add(poolObject);
        return poolObject;
    }

    public void Dispose()
    {
        _collector.Dispose();
        _poolObjects.ForEach(Object.Destroy);
    }
}

public class ConnectionCollector : IDisposable
{
    private LinkedList<IDisposable> _disposables = new LinkedList<IDisposable>();

    public IDisposable Add
    {
        set => _disposables.AddLast(value);
    }

    public void Dispose()
    {
        _disposables.ForEach(disposable => disposable.Dispose());
    }
}
public static class AnimatorExtensions
{
    public static readonly int MeleeAttack = Animator.StringToHash("MeleeAttack");
    public static readonly int FireAttack = Animator.StringToHash("FireAttack");
}