﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UniRx;
using UnityEngine;

public class InputControllerScript : MonoBehaviour
{
    public static InputControllerScript Instance;

    public JoystickInputController[] JoystickInputControllers;
    
    public ReactiveProperty<Vector2> PointerPosition = new ReactiveProperty<Vector2>();
    public ReactiveProperty<Vector2> MoveDirection = new ReactiveProperty<Vector2>();
    public OrSubstance PointerDown = new OrSubstance();
    
    public ReactiveProperty<Vector2> PointerDistance = new ReactiveProperty<Vector2>();
    
    private void Awake()
    {
        Instance = this;
#if PLATFORM_STANDALONE
        if (JoystickInputControllers != null)
        {
            foreach (var joystick in JoystickInputControllers)
            {
                joystick.SetActiveSafe(false);
            }
        }
#endif
    }

    private void Start()
    {
        //TODO: ConnectionCollector
        JoystickInputController.PressedJoysticksStatus[JoystickTypeEnum.Attack].Bind(PointerDown.Set);
    }

    void Update()
    {
#if PLATFORM_STANDALONE
        PointerPosition.Value = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MoveDirection.Value = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        PointerDown.Set(Input.GetMouseButton(0));
        var xCenter = (Screen.width/2f);
        var xDistance = (Input.mousePosition.x - xCenter) / xCenter;
        var yCenter = (Screen.height/2f);
        var yDistance = (Input.mousePosition.y - yCenter) / yCenter;
        PointerDistance.Value = new Vector2(xDistance, yDistance);
#elif UNITY_ANDROID
        PointerPosition.Value = JoystickInputController.JoystickValues[JoystickTypeEnum.Attack];
        MoveDirection.Value = JoystickInputController.JoystickValues[JoystickTypeEnum.Walk];
        PointerDistance.Value = JoystickInputController.JoystickValues[JoystickTypeEnum.Attack];
#endif
    }
}
