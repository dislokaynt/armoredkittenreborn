﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EffectsControllerScript : StandartLinksBehaviour
{
    public static EffectsControllerScript Instance;

    public Transform EffectsHolder;

    private void Awake()
    {
        Instance = this;
    }

    public List<EffectListData> Effects = new List<EffectListData>();
    
    public Dictionary<string, Pool<EffectInfo>> EffectsPools = new Dictionary<string, Pool<EffectInfo>>();

    public void SpawnEffect(string id, Vector3 position, float rotation, float timeToReload)
    {
        if(!EffectsPools.ContainsKey(id))
            EffectsPools[id] = new Pool<EffectInfo>(EffectsHolder,
                () => Instantiate((EffectInfo)Effects.First(effect => effect.EffectName == id).EffectPrefab), 1);
        var prefab = EffectsPools[id].GetPoolObject();
        prefab.transform.position = position;
        prefab.transform.eulerAngles =
            new Vector3(prefab.transform.eulerAngles.x, prefab.transform.eulerAngles.y, rotation);
        prefab.SetActiveSafe(true);
        //TODO: ConnectionCollector
        GameController.SetTimerFunction(new TimerFunction()
        {
            TimeLeft = timeToReload,
            FinalAction = () =>
            {
                prefab.SetActiveSafe(false);
            }
        });
    }
}

[Serializable]
public class EffectListData
{
    public string EffectName;
    public PoolObject EffectPrefab;
}

