﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class GameControllerScript : StandartLinksBehaviour
{
    public static GameControllerScript Instance;

    public GameDataInformation MainGameDataInformation;

    public Transform BulletsHolder;
    
    void Awake()
    {
        Instance = this;
    }

    private List<TimerFunction> _timerFunctions = new List<TimerFunction>();

    private void Update()
    {
        for (int i = 0; i < _timerFunctions.Count; i++)
        {
            _timerFunctions[i].TimeLeft -= Time.deltaTime;
            if (_timerFunctions[i].TimeLeft <= 0)
            {
                var function = _timerFunctions[i];
                _timerFunctions.RemoveAt(i);
                function.FinalAction.Invoke();
                i--;
            }
        }
    }

    public IDisposable SetTimerFunction(TimerFunction timerFunction)
    {
        _timerFunctions.Add(timerFunction);
        timerFunction.Started = true;
        return Disposable.Create(() => _timerFunctions.Remove(timerFunction));
    }

    public ActionStream FixedUpdateLoop = new ActionStream();
    
    private void FixedUpdate()
    {
        FixedUpdateLoop.Send();
    }
}

public class TimerFunction
{
    public float TimeLeft;
    public Action FinalAction;
    public bool Started; 
    public bool IsDone => TimeLeft <= 0;
}
