﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PoolObject : StandartLinksBehaviour
{
    public ActionStream ReturnToPoolStream = new ActionStream();

    private void OnDisable()
    {
        ReturnToPoolStream.Send();
        ReturnToPoolStream.Dispose();
    }
}
