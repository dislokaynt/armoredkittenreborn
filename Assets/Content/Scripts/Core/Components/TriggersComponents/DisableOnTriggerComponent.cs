﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnTriggerComponent : GameComponent
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        gameObject.SetActiveSafe(false);
    }
}
