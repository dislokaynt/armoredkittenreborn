﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAfterSecondsComponent : GameComponent
{
    public float TimeToDisable;
    private IDisposable _timerDisposable;
    private void OnEnable()
    {
        var timerFunction = new TimerFunction()
        {
            TimeLeft = TimeToDisable,
            FinalAction = () => gameObject.SetActiveSafe(false),
        };
        _timerDisposable = GameController.SetTimerFunction(timerFunction);
    }

    public void OnDisable()
    {
        _timerDisposable?.Dispose();
    }
}
