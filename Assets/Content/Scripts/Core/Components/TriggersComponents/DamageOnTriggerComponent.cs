﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnTriggerComponent : PoolGameComponent
{
    [SerializeField]
    private int _damage;

    private void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<HpComponent>()?.GetDamage(_damage, transform, DamageTypeEnum.Bullet);
    }
}
