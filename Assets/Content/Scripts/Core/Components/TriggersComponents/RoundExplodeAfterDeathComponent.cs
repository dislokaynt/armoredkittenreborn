﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class RoundExplodeAfterDeathComponent : GameComponent
{
    public int Damage;
    public float Range;

    public bool NeedGuiDraw = false;

    private void OnDrawGizmos()
    {
        if (!NeedGuiDraw)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, Range);
    }

    private void Start()
    {
        //TODO: ConnectionsCollector
        ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(AnalyzeHpMessages);
    }

    private void AnalyzeHpMessages(HpChangeMessage message)
    {
        if (message.HpStatus == HpActionsEnum.Death)
        {
            Physics2D.OverlapCircleAll(transform.position, Range).Select(target => target.GetComponent<HpComponent>())
                .Where(target => target != null).ForEach(target =>
                {
                    target.GetDamage(Damage, transform, DamageTypeEnum.Explosion);
                });
        }
    }
}
