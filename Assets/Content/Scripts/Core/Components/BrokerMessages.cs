﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMessage
{
    public GameComponent Sender;
}
public class HpChangeMessage : MainMessage
{
    public HpActionsEnum HpStatus;
    public DamageTypeEnum DamageType;
    public int Damage;
    public Transform DamageDealer;
}