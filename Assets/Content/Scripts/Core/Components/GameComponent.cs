﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class GameComponent : StandartLinksBehaviour, IInitable
{
    public MessageBroker ObjectMessageBroker => LocalMessageBrokerComponent.LocalBroker;
    public MessageBrokerComponent LocalMessageBrokerComponent;
    public ConnectionCollector Collector => LocalMessageBrokerComponent.DisposableCollector;

    public virtual void Init() { }
}
public class GameComponentWithDeathDisable : GameComponent
{
    protected ReactiveProperty<bool> _disabled = new ReactiveProperty<bool>(false);
    public override void Init()
    {
        _disabled.Value = false;
        Collector.Add = ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(message =>
        {
            if (message.HpStatus == HpActionsEnum.Death)
            {
                _disabled.Value = true;
            }
        });
    }
}
public class PoolGameComponent : PoolObject
{
    public MessageBroker LocalMessageBroker;
}
