﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class MessageBrokerComponent : GameComponent
{
    public ConnectionCollector DisposableCollector = new ConnectionCollector();
    
    private List<GameComponent> _allComponents = new List<GameComponent>();
    public readonly MessageBroker LocalBroker = new MessageBroker();

    private void Awake()
    {
        RegisterAllComponents();
    }

    private void RegisterAllComponents()
    {
        gameObject.GetComponentsInChildren<GameComponent>(_allComponents);
        _allComponents.Remove(this);
        for (int i = 0; i < _allComponents.Count; i++)
        {
            _allComponents[i].LocalMessageBrokerComponent = this;
        }
    }
    public override void Init()
    {
        _allComponents.ForEach(component => 
        {
            component.Init();
        });
    }
}
