﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ZombieAnimatorController : GameComponent
{
    public Animator ZombieAnimator;
    public CapsuleCollider2D ZombieCollider;

    [Header("Walk component")] 
    [SerializeField]
    private SpeedComponent _speedComponent;
    [SerializeField]
    private Rigidbody2D _body;
    
    [Header("Aim component")] 
    [SerializeField]
    private RotationComponent _rotationComponent = null;
    public override void Init()
    {
        Collector.Add = ObjectMessageBroker.Receive<WalkStatusesEnum>().Subscribe(AnalyzeWalkMessages);
        Collector.Add = ObjectMessageBroker.Receive<RotationStatusesEnum>().Subscribe(AnalyzeRotationMessages);
        Collector.Add = ObjectMessageBroker.Receive<FireStatusEnum>().Subscribe(AnalyzeFireMessages);
        ZombieAnimator.SetFloat("Aim", 0.5f);
        Collector.Add = ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(AnalyzeHpChangeMessages);
    }

    private void AnalyzeHpChangeMessages(HpChangeMessage message)
    {
        switch (message.HpStatus)
        {
            case HpActionsEnum.GetDamage:
            {
                if (message.Damage > 100)
                {
                    EffectsController.SpawnEffect("bloodStrong",
                        ZombieCollider.ClosestPoint(message.DamageDealer.position),
                        (Mathf.Rad2Deg * Utils.GetAngle(message.DamageDealer.position - transform.position)) - 90, 2f);
                }
                else
                {
                    EffectsController.SpawnEffect("bloodLight",
                        ZombieCollider.ClosestPoint(message.DamageDealer.position),
                        (Mathf.Rad2Deg * Utils.GetAngle(message.DamageDealer.position - transform.position)) - 90, 2f);
                }
                break;
            }
            case HpActionsEnum.Death:
            {
                ZombieAnimator.SetTrigger("Death");
                break;
            }
        }
    }
    
    private void AnalyzeWalkMessages(WalkStatusesEnum walkStatus)
    {
        switch (walkStatus)
        {
            case WalkStatusesEnum.BackWalk:
            {
                ZombieAnimator.SetBool("Back", true);
                break;
            }
            case WalkStatusesEnum.ForwardWalk:
            {
                ZombieAnimator.SetBool("Back", false);
                break;
            }
            case WalkStatusesEnum.StartWalk:
            {
                StartCoroutine(WalkSpeedChanger());
                break;
            }
            case WalkStatusesEnum.StopWalk:
            {
                StopCoroutine(WalkSpeedChanger());
                break;
            }
        }
    }

    private IEnumerator WalkSpeedChanger()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            ZombieAnimator.SetFloat("Velocity", _body.velocity.magnitude / (_speedComponent.Speed * Time.deltaTime));
        }
    }
    
    private void AnalyzeRotationMessages(RotationStatusesEnum walkStatus)
    {
        switch (walkStatus)
        {
            case RotationStatusesEnum.RotationStart:
            {
                StartCoroutine(RotationChanger());
                break;
            }
            case RotationStatusesEnum.RotationEnd:
            {
                StopCoroutine(RotationChanger());
                ZombieAnimator.SetFloat("Aim", 0.5f);
                break;
            }
        }
    }

    private void AnalyzeFireMessages(FireStatusEnum fireStatus)
    {
        switch (fireStatus)
        {
            case FireStatusEnum.Melee:
            {
                ZombieAnimator.SetTrigger("MeleeAttack");
                break;
            }
            case FireStatusEnum.Shot:{
                ZombieAnimator.SetTrigger("FireAttack");
                break;
            }
        }
    }

    private IEnumerator RotationChanger()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            var normalizedTime = Mathf.Abs(_rotationComponent.RotationProperty.Value / 180f);
            ZombieAnimator.SetFloat("Aim", normalizedTime);
        }
    }

}
