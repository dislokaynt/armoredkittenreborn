﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class KittenAnimatorController : GameComponent
{
    public Animator KittenAnimator;
    public CapsuleCollider2D KittenCollider;
    [SerializeField]
    private Transform _kittenCenter = null;

    [Header("Walk component")] 
    [SerializeField]
    private SpeedComponent _speedComponent = null;
    [SerializeField]
    private Rigidbody2D _body = null;
    
    [Header("Aim component")] 
    [SerializeField]
    private RotationComponent _rotationComponent = null;

    public void Start()
    {
        //TODO: убрать когда начнет появляться
        LocalMessageBrokerComponent.Init();
    }
    public override void Init()
    {
        Collector.Add = ObjectMessageBroker.Receive<WalkStatusesEnum>().Subscribe(AnalyzeWalkMessages);
#if !PLATFORM_STANDALONE
        Collector.Add = ObjectMessageBroker.Receive<RotationStatusesEnum>().Subscribe(AnalyzeRotationMessages);
#else
        StartCoroutine(RotationChanger());
#endif
        KittenAnimator.SetFloat("Aim", 0.5f);
        Collector.Add = ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(AnalyzeHpChangeMessages);
    }

    private void AnalyzeWalkMessages(WalkStatusesEnum walkStatus)
    {
        switch (walkStatus)
        {
            case WalkStatusesEnum.BackWalk:
            {
                KittenAnimator.SetBool("Back", true);
                break;
            }
            case WalkStatusesEnum.ForwardWalk:
            {
                KittenAnimator.SetBool("Back", false);
                break;
            }
            //TODO: If died when started
            case WalkStatusesEnum.StartWalk:
            {
                StartCoroutine(WalkSpeedChanger());
                break;
            }
            case WalkStatusesEnum.StopWalk:
            {
                StopCoroutine(WalkSpeedChanger());
                break;
            }
        }
    }

    private void AnalyzeHpChangeMessages(HpChangeMessage message)
    {
        switch (message.HpStatus)
        {
            case HpActionsEnum.GetDamage:
            {
                if (message.Damage > 100)
                {
                        var damagePosition = KittenCollider.ClosestPoint(message.DamageDealer.position);
                        EffectsController.SpawnEffect("bloodStrong",
                        damagePosition,
                        (Mathf.Rad2Deg * Utils.GetAngle((Vector2)message.DamageDealer.position - damagePosition)), 2f);
                }
                else
                    {
                        var damagePosition = KittenCollider.ClosestPoint(message.DamageDealer.position);
                        EffectsController.SpawnEffect("bloodLight",
                        damagePosition,
                        (Mathf.Rad2Deg * Utils.GetAngle((Vector2)message.DamageDealer.position - damagePosition)), 2f);
                }
                break;
            }
            case HpActionsEnum.Death:
            {
                KittenAnimator.SetTrigger("Death");
                break;
            }
        }
    }
    private IEnumerator WalkSpeedChanger()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            KittenAnimator.SetFloat("Velocity", _body.velocity.magnitude / (_speedComponent.Speed * Time.deltaTime));
        }
    }
    
    private void AnalyzeRotationMessages(RotationStatusesEnum walkStatus)
    {
        switch (walkStatus)
        {
            case RotationStatusesEnum.RotationStart:
            {
                StartCoroutine(RotationChanger());
                break;
            }
            case RotationStatusesEnum.RotationEnd:
            {
                StopCoroutine(RotationChanger());
                KittenAnimator.SetFloat("Aim", 0.5f);
                break;
            }
        }
    }

    private IEnumerator RotationChanger()
    {
        while (true)
        {
            yield return new WaitForFixedUpdate();
            var normalizedTime = Mathf.Abs(_rotationComponent.RotationProperty.Value / 180f);
            KittenAnimator.SetFloat("Aim", normalizedTime);
        }
    }
}
