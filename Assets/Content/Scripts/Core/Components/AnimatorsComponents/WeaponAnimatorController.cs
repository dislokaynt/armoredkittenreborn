﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class WeaponAnimatorController : GameComponent
{
    [Header("Shells part")]
    public int ShellsByShot;
    public ParticleSystem ShellsEmitter;
    
    [Header("Muzzle part")]
    public ParticleSystem MuzzleEmiter;

    public override void Init()
    {
        Collector.Add = ObjectMessageBroker.Receive<FireStatusEnum>().Subscribe(AnalyzeFireStatus);
    }

    private void AnalyzeFireStatus(FireStatusEnum status)
    {
        if (status == FireStatusEnum.Shot)
        {
            ShellsEmitter.Emit(ShellsByShot);
            MuzzleEmiter.Emit(1);
        }
    }
}
