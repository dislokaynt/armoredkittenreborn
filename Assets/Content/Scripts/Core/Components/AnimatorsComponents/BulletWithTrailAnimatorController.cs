﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWithTrailAnimatorController : GameComponent
{
    public TrailRenderer Trail;

    private void Awake()
    {
        if (Trail != null)
        {
            Trail.enabled = false;
        }
    }

    private void OnEnable()
    {
        if (Trail != null)
        {
            Trail.enabled = true;
            Trail.Clear();
        }
    }
}
