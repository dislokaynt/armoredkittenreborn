﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class BarrelAnimatorController : GameComponent
{
    private void Start()
    {
        //TODO: Временно, до того как начнут появляться по пути ходьбы
        LocalMessageBrokerComponent.Init();
    }
    public override void Init()
    {
        Collector.Add = ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(AnalyzeHpMessages);
    }

    void AnalyzeHpMessages(HpChangeMessage message)
    {
        switch (message.HpStatus)
        {
            case HpActionsEnum.Death:
            {
                EffectsController.SpawnEffect("barrelExplosion", transform.position, transform.eulerAngles.z, 1.3f);
                gameObject.SetActiveSafe(false);
                break;
            }
        }
    }
}
