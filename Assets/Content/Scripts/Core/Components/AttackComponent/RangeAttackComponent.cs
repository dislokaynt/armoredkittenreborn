﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class RangeAttackComponent : GameComponentWithDeathDisable
{
    [SerializeField] private TargetComponent _targetComponent;

    public WeaponComponent Weapon;

    public bool NeedGuiDraw = false;

    public HpComponent Target => _targetComponent.Target;

    public override void Init()
    {
        base.Init();
        Collector.Add = _disabled.Subscribe(val =>
        {
            if (val)
            {
                _isAttacking = false;
                ObjectMessageBroker.Publish(AttackStatusEnum.AttackEnd);
            }
        });
    }

    private void FixedUpdate()
    {
        if (_disabled.Value)
            return;
        CheckDistanceAndAttack();
    }

    private bool _isAttacking;

    private void CheckDistanceAndAttack()
    {
        if (!_isAttacking && Vector2.Distance(transform.position, Target.transform.position) < Weapon.AiAttackRange)
        {
            ObjectMessageBroker.Publish(AttackStatusEnum.AttackStart);
            _isAttacking = true;
        }
        else if (_isAttacking && Vector2.Distance(transform.position, Target.transform.position) > Weapon.AiAttackRange)
        {
            _isAttacking = false;
            ObjectMessageBroker.Publish(AttackStatusEnum.AttackEnd);
        }
    }

    private void OnDrawGizmos()
    {
        if (!NeedGuiDraw || Weapon == null)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, Weapon.AiAttackRange);
    }
}
