﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackComponent : GameComponent
{
    
}

public enum AttackStatusEnum
{
    AttackStart,
    AttackEnd,
}
public enum FireStatusEnum
{
    Shot,
    Melee,
}
public enum DamageTypeEnum
{
    Bullet,
    Explosion,
    Melee
}
