﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class HandsAttackComponent : GameComponentWithDeathDisable
{
    [SerializeField] private TargetComponent _targetComponent;
    public int Damage;
    public float Range;
    public float AttackCooldown;
    
    public bool NeedGuiDraw = false;
    
    public HpComponent Target => _targetComponent.Target;

    public override void Init()
    {
        base.Init();
        _isAttacking = false;
        Collector.Add = _disabled.Subscribe(val =>
        {
            if (val)
            {
                StopAllCoroutines();
            }
        });
    }

    private void FixedUpdate()
    {
        if (_disabled.Value)
            return;
        CheckDistanceAndAttack();
    }

    private bool _isAttacking;
    private Coroutine _attackCoroutine;
    private void CheckDistanceAndAttack()
    {
        if (!_isAttacking && Vector2.Distance(transform.position, Target.transform.position) < Range)
        {
            _attackCoroutine = StartCoroutine(AttackProcess());
            ObjectMessageBroker.Publish(AttackStatusEnum.AttackStart);
            _isAttacking = true;
        }
        else if (_isAttacking && Vector2.Distance(transform.position, Target.transform.position) > Range)
        {
            _isAttacking = false;
            StopCoroutine(_attackCoroutine);
            ObjectMessageBroker.Publish(AttackStatusEnum.AttackEnd);
        }
    }

    public IEnumerator AttackProcess()
    {
        while (true)
        {
            yield return Attack();
        }
    }

    private TimerFunction _cooldown;
    private IEnumerator Attack()
    {
        yield return new WaitWhile(() => _cooldown != null && !_cooldown.IsDone);
        
        ObjectMessageBroker.Publish(FireStatusEnum.Melee);
        if (_cooldown == null)
        {
            _cooldown = new TimerFunction {FinalAction = () => { }};
        }
        Target.GetDamage(Damage, transform, DamageTypeEnum.Melee);
        _cooldown.TimeLeft = AttackCooldown;
        GameController.SetTimerFunction(_cooldown);
        yield return new WaitWhile(() => _cooldown != null && !_cooldown.IsDone);
    }
    
    private void OnDrawGizmos()
    {
        if (!NeedGuiDraw)
            return;
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, Range);
    }
}
