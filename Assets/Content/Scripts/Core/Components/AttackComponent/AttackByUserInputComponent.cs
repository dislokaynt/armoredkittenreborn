﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackByUserInputComponent : GameComponent
{
    public override void Init()
    {
        Collector.Add = InputControllerScript.Instance.PointerDown.Bind(val=>
        {
            ObjectMessageBroker.Publish(val ? AttackStatusEnum.AttackStart : AttackStatusEnum.AttackEnd);
        });
    }
}
