﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class RotateAtTargetComponent : GameComponentWithDeathDisable
{
    [SerializeField] private RotationComponent _rotationComponent;
    [SerializeField] private TargetComponent _targetComponent;
    
    public Transform Target => _targetComponent.Target.transform;
    private bool _isRotating;

    public override void Init()
    {
        base.Init();
        Collector.Add = _disabled.Subscribe(val =>
        {
            if (val)
            {
                _isRotating = false;
                ObjectMessageBroker.Publish(RotationStatusesEnum.RotationEnd);
            }
        });
    }

    private void FixedUpdate()
    {
        if (_disabled.Value)
            return;
        if (!_isRotating && Target != null)
        {
            _isRotating = true;
            ObjectMessageBroker.Publish(RotationStatusesEnum.RotationStart);
        }
        else if (Target == false)
        {
            if (_isRotating)
            {
                _isRotating = false;
                ObjectMessageBroker.Publish(RotationStatusesEnum.RotationEnd);
            }
            return;
        }
        ChangeRotation();
    }

    private void ChangeRotation()
    {
        _rotationComponent.RotationProperty.Value =
            Utils.GetAngle(Target.position - transform.position) * Mathf.Rad2Deg - 90;
        _rotationComponent.RotationProperty.Value = _rotationComponent.RotationProperty.Value < -180
            ? _rotationComponent.RotationProperty.Value + 360
            : _rotationComponent.RotationProperty.Value;
    }
}
