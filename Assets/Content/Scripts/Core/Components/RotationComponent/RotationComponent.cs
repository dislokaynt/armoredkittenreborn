﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class RotationComponent : GameComponent
{
    public ReactiveProperty<float> RotationProperty = new ReactiveProperty<float>();
}

public enum RotationStatusesEnum
{
    RotationStart,
    RotationEnd,
    RotatedBack,
    RotatedFront,
}
