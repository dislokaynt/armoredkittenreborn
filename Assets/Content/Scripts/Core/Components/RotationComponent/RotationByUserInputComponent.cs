﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationByUserInputComponent : GameComponent
{
    public Transform AimCenter;
    [SerializeField] private RotationComponent _rotationComponent;

    private bool _rotatedBack;

    void Start()
    {
#if !PLATFORM_STANDALONE
        Collector.Add = InputControllerScript.Instance.PointerDown.Bind(val =>
        {
            ObjectMessageBroker.Publish(val ? RotationStatusesEnum.RotationStart : RotationStatusesEnum.RotationEnd);
        });
#endif
    }
    
    void FixedUpdate()
    {
        var pointerPosition = InputControllerScript.Instance.PointerPosition.Value;
#if PLATFORM_STANDALONE
        pointerPosition = pointerPosition - (Vector2) AimCenter.position;
#endif
        ChangeRotation(Utils.GetAngle(pointerPosition) * Mathf.Rad2Deg - 90);
    }

    void ChangeRotation(float rotation)
    {
        rotation = rotation < -180 ? rotation + 360 : rotation;
        _rotationComponent.RotationProperty.Value = rotation;
        var normalizedTime = rotation / 180;
        if (normalizedTime < 0)
        {
            if (_rotatedBack)
            {
                transform.localScale = new Vector3(1, 1, 1);
                _rotatedBack = false;
                ObjectMessageBroker.Publish(RotationStatusesEnum.RotatedFront);
            }
        }
        else
        {
            if (!_rotatedBack)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                _rotatedBack = true;
                ObjectMessageBroker.Publish(RotationStatusesEnum.RotatedBack);
            }
        }
    }
}