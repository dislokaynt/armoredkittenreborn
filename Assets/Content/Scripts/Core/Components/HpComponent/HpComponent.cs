﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class HpComponent : GameComponent
{
    [SerializeField] private int _maxHp = 0;
    public int MaxHp => _maxHp;
    private int _currentHp;
    public int CurrentHp => _currentHp;
    
    private bool _dead = false;

    [Header("Death disable")]
    public bool DisableAfterDeath;
    public bool NoCollisionAfterDeath;
    public float SecondsBeforeDisable;
    public bool DisconnectCollector;

    public override void Init()
    {
        _dead = false;
        _currentHp = _maxHp;
    }
    
    public void GetDamage(int damage, Transform damageDealer, DamageTypeEnum damageType)
    {
        ObjectMessageBroker.Publish(new HpChangeMessage()
        {
            HpStatus = HpActionsEnum.GetDamage,
            DamageType = damageType,
            Damage = damage,
            DamageDealer = damageDealer,
        });
        _currentHp -= damage;
        if (_currentHp < 0 && !_dead)
        {
            _dead = true;
            ObjectMessageBroker.Publish(new HpChangeMessage()
            {
                HpStatus = HpActionsEnum.Death,
                DamageType = damageType,
                Damage = damage,
                DamageDealer = damageDealer,
            });
            if (DisableAfterDeath)
            {
                //TODO: ConnectionCollector
                GameController.SetTimerFunction(new TimerFunction()
                {
                    FinalAction = () => gameObject.SetActiveSafe(false),
                    TimeLeft = SecondsBeforeDisable
                });
            }
            if (DisconnectCollector)
            {
                Collector.Dispose();
            }
            if (NoCollisionAfterDeath)
            {
                gameObject.layer = LayerMask.NameToLayer("NoCollisionLayer");
            }
        }
    }
}

public enum HpActionsEnum
{
    GetDamage,
    Death,
}
