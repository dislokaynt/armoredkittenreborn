﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class SubmachineWeaponComponent : WeaponComponent
{
    public PoolObject BulletPrefab;

    public float FireCooldown;

    private Pool<PoolObject> _bulletsPool;

    private Pool<PoolObject> BulletsPool => _bulletsPool ??
                                            (_bulletsPool = new Pool<PoolObject>(GameController.BulletsHolder,
                                                InstantiateAmmunition, 5));

    void Start()
    {
        //TODO: ConnectionsCollector
        ObjectMessageBroker.Receive<AttackStatusEnum>().Subscribe(AnalyzeAttackMessages);
    }

    private Coroutine _cashedProcess = null;

    private void AnalyzeAttackMessages(AttackStatusEnum attackStatus)
    {
        switch (attackStatus)
        {
            case AttackStatusEnum.AttackStart:
            {
                if (_cashedProcess != null)
                    return;
                _cashedProcess = StartCoroutine(FireProcess());
                break;
            }
            case AttackStatusEnum.AttackEnd:
            {
                if (_cashedProcess == null)
                    return;
                StopCoroutine(_cashedProcess);
                _cashedProcess = null;
                break;
            }
        }
    }

    public IEnumerator FireProcess()
    {
        while (true)
        {
            yield return Fire();
        }
    }

    private IEnumerator Fire()
    {
        yield return new WaitWhile(() => _cooldown != null && !_cooldown.IsDone);
        var obj = BulletsPool.GetPoolObject();
        obj.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + (transform.lossyScale.x > 0 ? 0 : 180));
        obj.transform.position = transform.position;
        obj.SetActiveSafe(true);
        
        ObjectMessageBroker.Publish(FireStatusEnum.Shot);
        if (_cooldown == null)
        {
            _cooldown = new TimerFunction {FinalAction = () => { }};
        }

        _cooldown.TimeLeft = FireCooldown;
        GameController.SetTimerFunction(_cooldown);
        yield return new WaitWhile(() => _cooldown != null && !_cooldown.IsDone);
    }

    private TimerFunction _cooldown;

    private PoolObject InstantiateAmmunition()
    {
        return Instantiate(BulletPrefab, GameController.BulletsHolder);
    }
}
