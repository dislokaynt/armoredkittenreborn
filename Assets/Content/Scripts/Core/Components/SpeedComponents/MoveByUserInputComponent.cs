﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class MoveByUserInputComponent : GameComponent
{
    public Rigidbody2D WalkBody;
    public SpeedComponent SpeedData;

    private bool _rotatedBack;

    public override void Init()
    {
        _stopped = true;
        Collector.Add = ObjectMessageBroker.Receive<RotationStatusesEnum>().Subscribe(AnalyzeRotationMessages);
    }

    void FixedUpdate()
    {
        Move(InputControllerScript.Instance.MoveDirection.Value);
    }

    void AnalyzeRotationMessages(RotationStatusesEnum rotationStatus)
    {
        switch (rotationStatus)
        {
            case RotationStatusesEnum.RotatedBack:
            {
                _rotatedBack = true;
                break;
            }
            case RotationStatusesEnum.RotatedFront:
            {
                _rotatedBack = false;
                break;
            }
        }
    }
  
    private bool _stopped = false;
    private bool _backWalk = false;
    void Move(Vector2 direction)
    {
        if (WalkBody == null || (direction == Vector2.zero && _stopped))
        {
            return;
        }
        if (_stopped && direction != Vector2.zero)
        {
            ObjectMessageBroker.Publish(WalkStatusesEnum.StartWalk);
            _stopped = false;
        }
        if (!_stopped && direction == Vector2.zero) 
        {
            ObjectMessageBroker.Publish(WalkStatusesEnum.StopWalk);
            _stopped = true;
        }
        WalkBody.velocity = direction * SpeedData.Speed * Time.deltaTime;
        if (!_backWalk && ((!_rotatedBack && direction.x < 0) || (_rotatedBack && direction.x > 0)))
        {
            ObjectMessageBroker.Publish(WalkStatusesEnum.BackWalk);
            _backWalk = true;
        }
        else if (_backWalk && ((!_rotatedBack && direction.x > 0) || (_rotatedBack && direction.x < 0)))
        {
            ObjectMessageBroker.Publish(WalkStatusesEnum.ForwardWalk);
            _backWalk = false;
        }
    }
}

public enum WalkStatusesEnum
{
    StartWalk,
    StopWalk,
    BackWalk,
    ForwardWalk,
}