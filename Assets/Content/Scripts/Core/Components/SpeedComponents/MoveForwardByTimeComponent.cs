﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoveForwardByTimeComponent : GameComponent
{
    [SerializeField] private SpeedComponent _speedComponent = null;

    private void FixedUpdate()
    {
        var diff = (Vector3)(Utils.GetAngle(transform.localEulerAngles.z) * _speedComponent.Speed * Time.fixedDeltaTime);
        var hits = Physics2D.RaycastAll(transform.position, diff, diff.magnitude);
        if (hits.Length > 0)
        {
            var hitted = hits.FirstOrDefault(hit => !Physics2D.GetIgnoreLayerCollision(gameObject.layer, hit.collider.gameObject.layer));
            if (hitted != default(RaycastHit2D))
            {
                transform.position = hitted.point;
                return;
            }
        }
        transform.position += diff;
    }
}
