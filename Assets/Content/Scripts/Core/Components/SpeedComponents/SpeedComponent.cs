﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedComponent : GameComponent
{
    [SerializeField] private float _speed = 0;

    public float Speed => _speed;
}
