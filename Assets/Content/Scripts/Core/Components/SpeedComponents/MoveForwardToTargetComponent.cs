﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class MoveForwardToTargetComponent : GameComponentWithDeathDisable
{    
    [SerializeField] private TargetComponent _targetComponent;
    public Transform Target => _targetComponent.Target.transform;

    public SpeedComponent SpeedData;
    public Rigidbody2D WalkBody;

    public bool StopWhenReadyToAttack;
    
    private bool _rotatedBack;

    public override void Init()
    {
        base.Init();
        _rotatedBack = false;
        _forceStop = false;
        _backWalk = false;
        _stopped = true;
        Collector.Add = ObjectMessageBroker.Receive<RotationStatusesEnum>().Subscribe(AnalyzeRotationMessages);
        if (StopWhenReadyToAttack)
            Collector.Add = ObjectMessageBroker.Receive<AttackStatusEnum>().Subscribe(AnalyzeAttackMessages);
    }

    private void FixedUpdate()
    {
        if (_disabled.Value)
            return;
        Move((Target.transform.position - transform.position).normalized);
    }

    void AnalyzeAttackMessages(AttackStatusEnum rotationStatus)
    {
        switch (rotationStatus)
        {
            case AttackStatusEnum.AttackStart:
            {
                _forceStop = true;
                _stopped = true;
                ObjectMessageBroker.Publish(WalkStatusesEnum.StopWalk);
                break;
            }
            case AttackStatusEnum.AttackEnd:
            {
                _forceStop = false;
                break;
            }
        }
    }
    
    void AnalyzeRotationMessages(RotationStatusesEnum rotationStatus)
    {
        switch (rotationStatus)
        {
            case RotationStatusesEnum.RotatedBack:
            {
                _rotatedBack = true;
                break;
            }
            case RotationStatusesEnum.RotatedFront:
            {
                _rotatedBack = false;
                break;
            }
        }
    }
  
    private bool _stopped = true;
    private bool _backWalk = false;
    private bool _forceStop = false;
    void Move(Vector2 direction)
    {
        if (!_backWalk && ((!_rotatedBack && direction.x < 0) || (_rotatedBack && direction.x > 0)))
        {
            transform.localScale = new Vector3(-1, 1, 1);
            _backWalk = true;
        }
        else if (_backWalk && ((!_rotatedBack && direction.x > 0) || (_rotatedBack && direction.x < 0)))
        {
            transform.localScale = new Vector3(1, 1, 1);
            _backWalk = false;
        }
        if (WalkBody == null || _forceStop || (direction == Vector2.zero && _stopped))
        {
            return;
        }

        if (_stopped)
            ObjectMessageBroker.Publish(WalkStatusesEnum.StartWalk);
        _stopped = direction == Vector2.zero;
        if (_stopped)
            ObjectMessageBroker.Publish(WalkStatusesEnum.StopWalk);
        
        WalkBody.velocity = direction * SpeedData.Speed * Time.deltaTime;
        
    }
}
