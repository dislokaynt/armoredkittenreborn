﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InfiniteMapTileComponent : PoolObject
{
    public Vector2 MapSize;
    public Vector2[] ObstacleSpawnPoints;
    public SpawnBound[] SpawnBounds;

    public (float, SpawnBound)[] GetPossibleSpawnBounds(SpawnerScript spawner)
    {
        var tempBounds = SpawnBounds.Select(bound => (GetSidesRelativeToSpawner(spawner, bound), bound))
            .Where(pair => (pair.Item1 & spawner.SpawnSides) != SpawnSidesEnum.None).ToArray();
        var sumOfLength = tempBounds.Sum(bound => bound.Item2.LengthOfBox);
        return tempBounds.Select(bound => (bound.Item2.LengthOfBox / sumOfLength, bound.Item2)).ToArray();
    }

    private SpawnSidesEnum GetSidesRelativeToSpawner(SpawnerScript spawner, SpawnBound bound)
    {
        var blocker = spawner.SpawnBlockerObject;
        var blockerZone = spawner.WithoutSpawnZone;
        var halfOfSides = new Vector2(blockerZone.x / 2, blockerZone.y / 2);
        var blockerTopLeft = new Vector2(blocker.position.x - halfOfSides.x, blocker.position.y + halfOfSides.y);
        var blockerDownRight = new Vector2(blocker.position.x + halfOfSides.x, blocker.position.y - halfOfSides.y);

        var spawnTopLeft = new Vector2(transform.position.x + bound.TopLeftCorner.x,
            transform.position.y - bound.TopLeftCorner.y);
        var spawnDownRight = new Vector2(transform.position.x + bound.DownRightCorner.x,
            transform.position.y - bound.DownRightCorner.y);

        var tempSides = SpawnSidesEnum.None;
        tempSides |= spawnTopLeft.y > blockerTopLeft.y
            ? SpawnSidesEnum.Top
            : SpawnSidesEnum.None;
        tempSides |= spawnDownRight.y < blockerDownRight.y 
            ? SpawnSidesEnum.Bottom
            : SpawnSidesEnum.None;
        tempSides |= spawnTopLeft.x < blockerTopLeft.x
            ? SpawnSidesEnum.Left
            : SpawnSidesEnum.None;
        tempSides |= spawnDownRight.x > blockerDownRight.x
            ? SpawnSidesEnum.Right
            : SpawnSidesEnum.None;
        return tempSides;
    }

    public Vector3 GetSpawnPosition()
    {
        Vector3 temp;
        do
        {
            var bound = GetPossibleSpawnBounds(InstanceController.Spawner).GetWeightedRandomElement();

            temp = transform.position +
                   new Vector3(UnityEngine.Random.Range(bound.TopLeftCorner.x, bound.DownRightCorner.x),
                       UnityEngine.Random.Range(bound.TopLeftCorner.y, bound.DownRightCorner.y), 0);
        } while (NeedNewPostion(temp, InstanceController.Spawner));

        return temp;
    }

    public bool NeedNewPostion(Vector3 position, SpawnerScript spawner)
    {
        var halfOfZone = new Vector2(spawner.WithoutSpawnZone.x / 2, spawner.WithoutSpawnZone.y / 2);
        var zoneTopLeftBound = new Vector2(spawner.SpawnBlockerObject.position.x - halfOfZone.x, spawner.SpawnBlockerObject.position.y + halfOfZone.y);
        var zoneDownRightBound = new Vector2(spawner.SpawnBlockerObject.position.x + halfOfZone.x, spawner.SpawnBlockerObject.position.y - halfOfZone.y);
        if (zoneTopLeftBound.x < position.x && zoneDownRightBound.x > position.x &&
            zoneTopLeftBound.y > position.y && zoneDownRightBound.y < position.y)
            return true;
        var hits = Physics.RaycastAll(new Vector3(position.x, position.y, 1), position, 3f);
        return hits.Any(hit => hit.collider != null);
    }

#if UNITY_EDITOR
    [Header("InspectorDraw")]
    public bool NeedToGuiDraw;
    
    public bool DrawSpawnZones;
    public bool DrawTileSize;
    public bool DrawObstaclePoints;
    private void OnDrawGizmos()
    {
        if (NeedToGuiDraw)
        {
            if (DrawSpawnZones)
            {
                Gizmos.color = Color.green;
                foreach (var spawnBound in SpawnBounds)
                {
                    var spawnTopLeft = new Vector2(transform.position.x + spawnBound.TopLeftCorner.x,
                        transform.position.y - spawnBound.TopLeftCorner.y);
                    var spawnDownRight = new Vector2(transform.position.x + spawnBound.DownRightCorner.x,
                        transform.position.y - spawnBound.DownRightCorner.y);
                    
                    Gizmos.DrawLine(spawnTopLeft,
                        new Vector2(spawnDownRight.x, spawnTopLeft.y));
                    Gizmos.DrawLine(new Vector2(spawnDownRight.x, spawnTopLeft.y),
                        spawnDownRight);
                    Gizmos.DrawLine(spawnDownRight,
                        new Vector2(spawnTopLeft.x, spawnDownRight.y));
                    Gizmos.DrawLine(new Vector2(spawnTopLeft.x, spawnDownRight.y),
                        spawnTopLeft);
                }
            }

            if (DrawTileSize)
            {
                Gizmos.color = Color.magenta;
                var UpperLeftPosition =
                    new Vector2(transform.position.x - MapSize.x / 2f, transform.position.y + MapSize.y / 2f);
                var LowerRightPosition =
                    new Vector2(transform.position.x + MapSize.x / 2f, transform.position.y - MapSize.y / 2f);
                Gizmos.DrawLine(UpperLeftPosition,
                    new Vector2(LowerRightPosition.x, UpperLeftPosition.y));
                Gizmos.DrawLine(new Vector2(LowerRightPosition.x, UpperLeftPosition.y),
                    LowerRightPosition);
                Gizmos.DrawLine(LowerRightPosition,
                    new Vector2(UpperLeftPosition.x, LowerRightPosition.y));
                Gizmos.DrawLine(new Vector2(UpperLeftPosition.x, LowerRightPosition.y),
                    UpperLeftPosition);
            }

            if (DrawObstaclePoints)
            {
                Gizmos.color = Color.red;
                foreach (var spawnPoint in ObstacleSpawnPoints)
                {
                    var spawnTemp = new Vector2(transform.position.x + spawnPoint.x,
                        transform.position.y - spawnPoint.y);

                    Gizmos.DrawSphere(spawnTemp, 1f);
                }
            }
        }
    }
#endif
    [Serializable]
    public class SpawnBound
    {
        public Vector2 TopLeftCorner;
        public Vector2 DownRightCorner;

        private bool _asighed;
        private float _lengthOfBox;

        public float LengthOfBox
        {
            get
            {
                if (!_asighed)
                {
                    _lengthOfBox = CalculateLength();
                    _asighed = true;
                }
                return _lengthOfBox;
            }
        }
        
        private float CalculateLength()
        {
            return Math.Abs(TopLeftCorner.x - DownRightCorner.x) + Math.Abs(TopLeftCorner.y - DownRightCorner.y);
        }
    }
}
