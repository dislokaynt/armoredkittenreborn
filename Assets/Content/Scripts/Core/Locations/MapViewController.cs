﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapViewController : StandartLinksBehaviour
{
    [SerializeField]
    protected Vector2 _upperLeftCameraPosition;
    public Vector2 UpperLeftCameraPosition => _upperLeftCameraPosition + (Vector2) transform.position;
    [SerializeField]
    protected Vector2 _lowerRightCameraPosition;
    public Vector2 LowerRightCameraPosition => _lowerRightCameraPosition + (Vector2) transform.position;

    public MapType MapTypeEnum;

    public CameraMapBounds CameraBounds;
    
    public Transform EnemiesHolder;
    
#if UNITY_EDITOR
    public bool NeedToGuiDraw;    
#endif
    public virtual void InitMap()
    {

    }
    public void SetAllChildrenAlive()
    {
       // gameObject.GetComponentsInChildren<IWakeUpable>().ForEach(child=>child.WakeUp());
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (NeedToGuiDraw)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(UpperLeftCameraPosition,
                new Vector2(LowerRightCameraPosition.x, UpperLeftCameraPosition.y));
            Gizmos.DrawLine(new Vector2(LowerRightCameraPosition.x, UpperLeftCameraPosition.y),
                LowerRightCameraPosition);
            Gizmos.DrawLine(LowerRightCameraPosition,
                new Vector2(UpperLeftCameraPosition.x, LowerRightCameraPosition.y));
            Gizmos.DrawLine(new Vector2(UpperLeftCameraPosition.x, LowerRightCameraPosition.y),
                UpperLeftCameraPosition);
        }
    }
#endif
}

[Flags]
public enum CameraMapBounds
{
    None = 0,
    Top = 1 << 0,
    Bottom = 1 << 1,
    Left = 1 << 2,
    Right = 1 << 3, 
}
