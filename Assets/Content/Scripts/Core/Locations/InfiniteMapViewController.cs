﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InfiniteMapViewController : MapViewController
{
    public Pool<InfiniteMapTileComponent> TilemapPool;
    public Transform TilemapHolder;
    public List<InfiniteMapTileComponent> TilemapPrefabs;
    public InfiniteMapTileComponent LeftMapPart;

    public Collider2D LeftCollider;

    [Range(0,1f)]
    public float ObstaclesSpawnFrequency;
    public ObstacleSpawnSetting[] ObstacleSpawnSettings;
    public ActionStream<InfiniteMapTileComponent> NextMapPart = new ActionStream<InfiniteMapTileComponent>();

    public override void InitMap()
    {
        for (int i = 0; i < ObstacleSpawnSettings.Length; i++)
        {
            var temp = i;
            ObstacleSpawnSettings[i].ObstaclePool = new Pool<PoolObject>(EnemiesHolder, () => Instantiate(ObstacleSpawnSettings[temp].ObstaclePrefab), 1);
        }
        TilemapPool = new Pool<InfiniteMapTileComponent>(TilemapHolder, () => Instantiate(TilemapPrefabs.GetRandomElement()), 3);
        NearBlocks.Add(LeftMapPart);
        NearBlocks.Add(TilemapPool.GetPoolObject(SetBlock));
        NearBlocks.Add(TilemapPool.GetPoolObject(SetBlock));
        SetAllChildrenAlive();
    }

    public Vector3 GetSpawnPosition()
    {
        var spawner = InstanceController.Spawner;
        var possibleSides = spawner.SpawnSides;
        return TilemapPool.ActiveObjects.Where(block =>
        {
            if (possibleSides.HasFlag(SpawnSidesEnum.Left) && block.transform.position.x < (InstanceController.PlayerTransform.position.x - spawner.WithoutSpawnZone.x / 2))
            {
                return true;
            }
            if (possibleSides.HasFlag(SpawnSidesEnum.Right) && block.transform.position.x > (InstanceController.PlayerTransform.position.x + spawner.WithoutSpawnZone.x / 2))
            {
                return true;
            }
            if (possibleSides.HasFlag(SpawnSidesEnum.Bottom) && block.transform.position.y < (InstanceController.PlayerTransform.position.y - spawner.WithoutSpawnZone.y / 2))
            {
                return true;
            }
            if (possibleSides.HasFlag(SpawnSidesEnum.Top) && block.transform.position.y > (InstanceController.PlayerTransform.position.y + spawner.WithoutSpawnZone.y / 2))
            {
                return true;
            }
            return false;
        }).GetRandomElement().GetSpawnPosition();
    }

    List<InfiniteMapTileComponent> NearBlocks = new List<InfiniteMapTileComponent>();
    private void FixedUpdate()
    {
        var playerPos = InstanceController.PlayerTransform.transform.position;
        if (playerPos.x > NearBlocks[1].transform.position.x + NearBlocks[1].MapSize.x / 2)
        {
            NearBlocks[0].SetActiveSafe(false);
            NearBlocks.RemoveAt(0);
            NearBlocks.Add(TilemapPool.GetPoolObject(SetBlock));
        }
    }

    public void SetBlock(InfiniteMapTileComponent mapPart)
    {
        SetBlockToPosition(mapPart);
        InstantiateAllObstacles(mapPart);
    }

    private void InstantiateAllObstacles(InfiniteMapTileComponent mapPart)
    {
        //TODO: Возвращение в пул, если кусок карты выключился
        var count = Mathf.Round(mapPart.ObstacleSpawnPoints.Length * ObstaclesSpawnFrequency);
        var usedSpawnPoints = new List<int>();
        for (int i = 0; i < count; i++)
        {
            var randomedIndex = UnityEngine.Random.Range(0, mapPart.ObstacleSpawnPoints.Length);
            while (usedSpawnPoints.Contains(randomedIndex))
                randomedIndex = UnityEngine.Random.Range(0, mapPart.ObstacleSpawnPoints.Length);
            usedSpawnPoints.Add(randomedIndex);
            var obstale = ObstacleSpawnSettings.GetWeightedRandomElement(element => element.SpawnChance).ObstaclePool.GetPoolObject();
            obstale.transform.position = (Vector2)mapPart.transform.position - mapPart.ObstacleSpawnPoints[randomedIndex];
            obstale.gameObject.layer = LayerMask.NameToLayer("EnemyLayer");
            obstale.GetComponent<MessageBrokerComponent>().Init();
            obstale.SetActiveSafe(true);
        }
    }

    private void SetBlockToPosition(InfiniteMapTileComponent mapPart)
    {
        var lastBlock = NearBlocks.Last();
        mapPart.transform.position = new Vector2(lastBlock.transform.position.x + lastBlock.MapSize.x / 2f
                                                                                + mapPart.MapSize.x / 2f,
            lastBlock.transform.position.y);
        mapPart.SetActiveSafe(true);
        UpdateLeftBounds();
        NextMapPart.Send(mapPart);
    }

    private void UpdateLeftBounds()
    {
        var firstBlock = NearBlocks.First();
        LeftCollider.transform.position = new Vector2(firstBlock.transform.position.x - firstBlock.MapSize.x / 2,
            firstBlock.transform.position.y);
        _upperLeftCameraPosition = new Vector2(firstBlock.transform.localPosition.x - firstBlock.MapSize.x / 2,
            _upperLeftCameraPosition.y);
    }
}
[Serializable]
public class ObstacleSpawnSetting
{
    public PoolObject ObstaclePrefab;
    [NonSerialized]
    public Pool<PoolObject> ObstaclePool;
    [Range(0,1f)]
    public float SpawnChance;
}
