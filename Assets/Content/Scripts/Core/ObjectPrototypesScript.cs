﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPrototypesScript : MonoBehaviour
{
    [SerializeField]
    private List<ObjectPrototypeInfo> _objectPrototypes;

    public static ObjectPrototypesScript Instance;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject GetPrototypeById(string id)
    {
        return _objectPrototypes.FirstOrDefault(info => info.Id == id)?.Prototype;
    }
    [Serializable]
    public class ObjectPrototypeInfo
    {
        public string Id;
        public GameObject Prototype;
    }
}

