﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class IdContainerComponent : GameComponent
{
    public string Id;

    private void OnEnable()
    {
        //TODO: ConnectionCollector
        ObjectMessageBroker.Receive<HpChangeMessage>().Subscribe(hpMessage =>
        {
            if (hpMessage.HpStatus == HpActionsEnum.Death)
            {
                InstanceController.DeathStream.Send((this, (HpComponent) hpMessage.Sender));
            }
        });
    }
}
