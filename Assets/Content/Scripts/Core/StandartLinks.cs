﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartLinks
{
    public GameDataInformation GameData => GameControllerScript.Instance.MainGameDataInformation;
    public GameControllerScript GameController => GameControllerScript.Instance;
    public ObjectPrototypesScript ObjectsPrototypes => ObjectPrototypesScript.Instance;
    public InstanceControllerScript InstanceController => InstanceControllerScript.Instance;
    public InputControllerScript InputController => InputControllerScript.Instance;
    public EffectsControllerScript EffectsController => EffectsControllerScript.Instance;
}
public class StandartLinksBehaviour : MonoBehaviour
{
    public GameDataInformation GameData => GameControllerScript.Instance.MainGameDataInformation;
    public GameControllerScript GameController => GameControllerScript.Instance;
    public ObjectPrototypesScript ObjectsPrototypes => ObjectPrototypesScript.Instance;
    public InstanceControllerScript InstanceController => InstanceControllerScript.Instance;
    public InputControllerScript InputController => InputControllerScript.Instance;
    public EffectsControllerScript EffectsController => EffectsControllerScript.Instance;
}

public class StandartLinksScriptable : ScriptableObject
{
    public GameDataInformation GameData => GameControllerScript.Instance.MainGameDataInformation;
    public GameControllerScript GameController => GameControllerScript.Instance;
    public ObjectPrototypesScript ObjectsPrototypes => ObjectPrototypesScript.Instance;
    public InstanceControllerScript InstanceController => InstanceControllerScript.Instance;
    public InputControllerScript InputController => InputControllerScript.Instance;
    public EffectsControllerScript EffectsController => EffectsControllerScript.Instance;
}

