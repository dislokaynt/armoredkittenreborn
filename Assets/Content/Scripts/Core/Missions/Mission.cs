﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission : StandartLinksScriptable
{
    public SpawnSidesEnum SpawnSides;
    public EnemySpawnType SpawnType;
    
    public List<MissionEnemyInfo> EnemyInfos;

    public bool HasIcon;
    public virtual string IconName { get; }
    public virtual int NeedTargetCount { get; }
    public virtual int CurrentCount { get; }
    public virtual IActionStream CurrentValueChanged { get; }

    public virtual void Init()
    {
        
    }
    
    [Serializable]
    public class MissionEnemyInfo
    {
        public float Chance;
        public PoolObject Prototype;
    }
}

[Flags] 
public enum SpawnSidesEnum
{
    None = 0,
    Left = 1 << 0,
    Right = 1 << 1,
    Top = 1 << 2,
    Bottom = 1 << 3,
}
public enum EnemySpawnType
{
    None = 0,
    OnNewBlock = 1,
    ByDistance = 2,
    Waves = 3,
    WavesAfterPreviousDeath = 4,
}
