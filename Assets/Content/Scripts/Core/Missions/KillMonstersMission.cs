﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "KillMonstersMission", menuName = "ScriptableObjects/Missions/KillMonstersMission")]
public class KillMonstersMission : Mission
{
    public string MonsterToKillId;
    public int MonstersToKillCount;

    [NonSerialized]
    private int _killedMonstersCount;
    
    public override int NeedTargetCount => MonstersToKillCount;
    public override int CurrentCount => _killedMonstersCount;

    private readonly IActionStream _currentValueChanged = new ActionStream();
    public override IActionStream CurrentValueChanged => _currentValueChanged;

    public override void Init()
    {
        //TODO: ConnectionCollector
        InstanceController.DeathStream.Listen(deathData =>
        {
            if (deathData.Item1.Id == MonsterToKillId)
            {
                _killedMonstersCount++;
                ((ActionStream) _currentValueChanged).Send();
            }
        });
    }
    
}
