﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RunDistanceMission", menuName = "ScriptableObjects/Missions/RunDistanceMission")]
public class RunDistanceMission : Mission
{
    public float DistanceScale;
    public int DistanceToRun;

    [NonSerialized]
    private int _lastMaximumDistance = 0;
    
    public override int NeedTargetCount => DistanceToRun;
    public override int CurrentCount
    {
        get
        {
            var tempDistance = (int) (InstanceController.PlayerDistanceFromStart * DistanceScale);
            if (tempDistance > _lastMaximumDistance)
                _lastMaximumDistance = tempDistance;
            return _lastMaximumDistance;
        }
    }

    public override IActionStream CurrentValueChanged => GameController.FixedUpdateLoop;
}
