﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

public class InstanceControllerScript : StandartLinksBehaviour
{
    public static InstanceControllerScript Instance;

    public Transform PlayerTransform;

    public float PlayerDistanceFromStart => PlayerTransform.position.x - _playerStartPosition.x;
    private Vector2 _playerStartPosition;

    public MapType CurrentMapType;

    public MapViewController CurrentMap;

    public MainUI InGameUI;

    public Mission CurrentMission;
    public SpawnerScript Spawner = new SpawnerScript();
    
    public ActionStream<(IdContainerComponent, HpComponent)> DeathStream = new ActionStream<(IdContainerComponent, HpComponent)>();

    private void Awake()
    {
        CurrentMapType = MapType.Hardcoded;
        Instance = this;
    }

    void Start()
    {
        InitializeFight();
    }

    public void InitializeFight()
    {
        _playerStartPosition = PlayerTransform.position;
        CurrentMission.Init();
        CurrentMap.InitMap();
        InGameUI.InitView();
        InitializeSpawner();
    }

    public void InitializeSpawner()
    {
        Spawner.FirstParent = Spawner.SecondParent = this;
        Spawner.Init(CurrentMap, CurrentMission);
    }
    
    private void OnDrawGizmos()
    {
        if (Spawner.NeedToGuiDraw)
        {
            Gizmos.color = Color.red;
            var UpperLeftPosition =
                new Vector2(PlayerTransform.transform.position.x - Spawner.WithoutSpawnZone.x / 2f,
                    PlayerTransform.transform.position.y + Spawner.WithoutSpawnZone.y / 2f);
            var LowerRightPosition =
                new Vector2(PlayerTransform.transform.position.x + Spawner.WithoutSpawnZone.x / 2f,
                    PlayerTransform.transform.position.y - Spawner.WithoutSpawnZone.y / 2f);
            Gizmos.DrawLine(UpperLeftPosition,
                new Vector2(LowerRightPosition.x, UpperLeftPosition.y));
            Gizmos.DrawLine(new Vector2(LowerRightPosition.x, UpperLeftPosition.y),
                LowerRightPosition);
            Gizmos.DrawLine(LowerRightPosition,
                new Vector2(UpperLeftPosition.x, LowerRightPosition.y));
            Gizmos.DrawLine(new Vector2(UpperLeftPosition.x, LowerRightPosition.y),
                UpperLeftPosition);
        }
    }

}
[Serializable]
public class SpawnerScript : IParrentable<InstanceControllerScript, InstanceControllerScript>
{
    public bool NeedToGuiDraw;
    [NonSerialized]
    public SpawnSidesEnum SpawnSides;

    public Vector2 WithoutSpawnZone;
    public Transform SpawnBlockerObject;

    public MapViewController Map;

    private List<(float, Pool<PoolObject>)> _enemiesPools = new List<(float, Pool<PoolObject>)>();

    public void Init(MapViewController map, Mission currentMission)
    {
        SpawnSides = currentMission.SpawnSides;
        Map = map;
        for (int i = 0; i < currentMission.EnemyInfos.Count; i++)
        {
            var enemyInfo = currentMission.EnemyInfos[i];
            _enemiesPools.Add((enemyInfo.Chance, new Pool<PoolObject>(FirstParent.CurrentMap.EnemiesHolder,
                    () => Object.Instantiate(enemyInfo.Prototype), 0)
                ));
        }
        //TODO: ConnectionCollector
        var spawnAction = InitSpawnActionByMission(currentMission);
    }

    public IDisposable InitSpawnActionByMission(Mission mission)
    {
        switch (mission.SpawnType)
        {
            case EnemySpawnType.OnNewBlock:
                {
                    var disp = ((InfiniteMapViewController)Map).NextMapPart.Listen(part =>
                       {
                           var zombiesTest = UnityEngine.Random.Range(3, 6);
                           for (int i = 0; i < zombiesTest; i++)
                           {
                               var enemyPool = _enemiesPools.GetWeightedRandomElement();
                               var enemy = enemyPool.GetPoolObject();
                               enemy.transform.position = part.GetSpawnPosition();

                               enemy.gameObject.layer = LayerMask.NameToLayer("EnemyLayer");
                               //TODO: лолкек, не забудь потом удалить
                               enemy.GetComponent<TargetComponent>().Target = FirstParent.InGameUI.HealthView.HpTarget;
                               enemy.SetActiveSafe(true);
                               enemy.GetComponent<MessageBrokerComponent>().Init();
                           }
                       }
                    );
                    return disp;
                }
            case EnemySpawnType.ByDistance:
                {
                    //ByTime
                    //var spawnReload = new TimerFunction()
                    //{ TimeLeft = 3f };
                    //if (!spawnReload.Started)
                    //    GameControllerScript.Instance.SetTimerFunction(spawnReload);
                    //if (!spawnReload.IsDone)
                    //    return;
                    //else
                    //{
                    //    spawnReload = new TimerFunction() { TimeLeft = 3f };
                    //    GameControllerScript.Instance.SetTimerFunction(spawnReload);
                    //}
                    var lastSpawnDistance = 0f;
                    var disp = GameControllerScript.Instance.FixedUpdateLoop.Listen(() =>
                    {
                        //TODO: впихнуть сюда возможность баланса
                        if (FirstParent.PlayerDistanceFromStart - lastSpawnDistance < 3f)
                        {
                            return;
                        }
                        lastSpawnDistance = FirstParent.PlayerDistanceFromStart;
                        var zombiesTest = UnityEngine.Random.Range(3, 6);
                        for (int i = 0; i < zombiesTest; i++)
                        {
                            var enemyPool = _enemiesPools.GetWeightedRandomElement();
                            var enemy = enemyPool.GetPoolObject();
                            enemy.transform.position = ((InfiniteMapViewController)Map).GetSpawnPosition();

                            enemy.gameObject.layer = LayerMask.NameToLayer("EnemyLayer");
                            //TODO: лолкек, не забудь потом удалить
                            enemy.GetComponent<TargetComponent>().Target = FirstParent.InGameUI.HealthView.HpTarget;
                            enemy.SetActiveSafe(true);
                            enemy.GetComponent<MessageBrokerComponent>().Init();
                        }
                    }
                    );
                    return disp;
                }                
        }
        return null;
    }
    
    public InstanceControllerScript FirstParent { get; set; }
    public InstanceControllerScript SecondParent { get; set; }
}
public enum MapType
{
    Scroller,
    Rooms,
    Hardcoded,
}