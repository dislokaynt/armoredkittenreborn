﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameDataInformation", menuName = "ScriptableObjects/BasicDesign/GameDataInformation")]
public class GameDataInformation : ScriptableObject
{
    public float DefaultKittenSpeed;
    public int DefaultKittenHp;
    public float DefaultKittenStrength;
}
