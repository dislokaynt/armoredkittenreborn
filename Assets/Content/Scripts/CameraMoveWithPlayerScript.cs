﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CameraMoveWithPlayerScript : StandartLinksBehaviour
{
    public Transform Player;
    public Vector2 CameraMaxDistance;
    public Camera CurrentCamera;
    public float CameraSpeed;
    private Vector3 _lastPosition;

    private Vector2 _cameraWidthHeight;
    private void Start()
    {
        _cameraWidthHeight = transform.position - CurrentCamera.ScreenToWorldPoint(new Vector3(0, 0, 0));
    }

    private void FixedUpdate()
    {
        _lastPosition = transform.position;
        var neededPos = GetNewRangedPosition((Vector2) Player.transform.position +
                                             (CameraMaxDistance * InputController.PointerDistance.Value));
        transform.position = Vector3.LerpUnclamped(_lastPosition,
            new Vector3(neededPos.x, neededPos.y, _lastPosition.z),
            CameraSpeed);
    }

    private Vector2 GetNewRangedPosition(Vector2 tempVector)
    {
        var currentMap = InstanceController.CurrentMap;
        
        var upperLeftPosition = currentMap.UpperLeftCameraPosition;
        var lowerRightPosition = currentMap.LowerRightCameraPosition;
        var x = tempVector.x - _cameraWidthHeight.x < upperLeftPosition.x && currentMap.CameraBounds.HasFlag(CameraMapBounds.Left)
            ? upperLeftPosition.x + _cameraWidthHeight.x
            : (tempVector.x + _cameraWidthHeight.x > lowerRightPosition.x) && currentMap.CameraBounds.HasFlag(CameraMapBounds.Right)
                ? lowerRightPosition.x - _cameraWidthHeight.x
                : tempVector.x;
        var y = tempVector.y + _cameraWidthHeight.y > upperLeftPosition.y && currentMap.CameraBounds.HasFlag(CameraMapBounds.Top)
            ? upperLeftPosition.y - _cameraWidthHeight.y
            : (tempVector.y - _cameraWidthHeight.y < lowerRightPosition.y) && currentMap.CameraBounds.HasFlag(CameraMapBounds.Bottom)
                ? lowerRightPosition.y + _cameraWidthHeight.y
                : tempVector.y;
        tempVector = new Vector2(x, y);
        return tempVector;
    }
}
