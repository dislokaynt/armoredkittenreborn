﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEditor;
using UnityEngine;

public class ActionStream<T> : IActionStream<T>, IDisposable
{
    private DisposableConnectionsList<T> _disposableActions;
    public ActionStream()
    {
        _disposableActions = new DisposableConnectionsList<T>();
    }
    public IDisposable Listen(Action<T> act)
    {
        return _disposableActions.Add(act);
    }

    public IDisposable Listen(Action act)
    {
        return _disposableActions.Add(_ => act());
    }

    public void Send(T value)
    {
        if (_disposableActions.Count < 1)
            return;
        _disposableActions.React(value);
    }

    public void Dispose()
    {
        _disposableActions.Clear();
    }

}

public class ActionStream : IActionStream<EmptyClass>, IDisposable
{
    private DisposableConnectionsList<EmptyClass> _disposableActions;
    public ActionStream()
    {
        _disposableActions = new DisposableConnectionsList<EmptyClass>();
    }
    public IDisposable Listen(Action<EmptyClass> act)
    {
        return _disposableActions.Add(act);
    }

    public IDisposable Listen(Action act)
    {
        return _disposableActions.Add(_ => act());
    }

    public void Send(EmptyClass value)
    {
        if (_disposableActions.Count < 1)
            return;
        _disposableActions.React(value);
    }
    
    public void Send()
    {
        if (_disposableActions.Count < 1)
            return;
        _disposableActions.React(null);
    }

    public void Dispose()
    {
        _disposableActions.Clear();
    }

}

public class DisposableConnectionsList<T>
{
    private readonly List<Action<T>> _disposableActions;

    public DisposableConnectionsList()
    {
        _disposableActions = new List<Action<T>>();
    }
    public IDisposable Add(Action<T> item)
    {
        _disposableActions.Add(item);
        return new AnonymousDisposable(() => _disposableActions.Remove(item));
    }    

    public void Clear()
    {
        _disposableActions.Clear();
    }

    public void React(T value)
    {
        _disposableActions.ForEach(action => action(value));
    }

    public int Count
    {
        get { return _disposableActions.Count; }
    }
}

public class AnonymousDisposable : IDisposable
{
    private bool _isDisposed = false;
    private readonly Action _dispose;

    public AnonymousDisposable(Action dispose)
    {
        _dispose = dispose;
    }

    public void Dispose()
    {
        if (!_isDisposed)
        {
            _isDisposed = true;
            _dispose();
        }
    }
}

public interface IActionStream
{
    IDisposable Listen(Action act);
}

public interface IActionStream<T> : IActionStream
{
    IDisposable Listen(Action<T> act);
}

public class OrSubstance : IActionStream<bool>, IDisposable
{
    private bool _value;

    public bool Value => _value;

    private DisposableConnectionsList<bool> _disposableActions;
    public OrSubstance()
    {
        _disposableActions = new DisposableConnectionsList<bool>();
    }
    public IDisposable Listen(Action<bool> act)
    {
        return _disposableActions.Add(act);
    }
    public IDisposable Bind(Action<bool> act)
    {
        act.Invoke(_value);
        return _disposableActions.Add(act);
    }

    public IDisposable Listen(Action act)
    {
        return _disposableActions.Add(_ => act());
    }

    public void Set(bool value)
    {
        if (_value == value)
            return;
        _value = value;
        if (_disposableActions.Count < 1)
            return;
        _disposableActions.React(value);
    }

    public void Dispose()
    {
        _disposableActions.Clear();
    }
}

public class ValueSubstance<T> : ReactiveProperty<T>
{
    private ReactiveProperty<T> _property = new ReactiveProperty<T>();

    public ReactiveProperty<T> Property => _property;

    private List<Func<T,T>> _disposableActions;
    public ValueSubstance()
    {
        _disposableActions = new List<Func<T,T>>();
    }

    public IDisposable OverrideValue(Func<T,T> overrideAction)
    {
        _disposableActions.Add(overrideAction);
        _property.Value = RecalculateValue(_property.Value);
        return Disposable.Create(() => _disposableActions.Remove(overrideAction));
    }
    
    public void Set(T value)
    {
        if (_property.Value.Equals(value))
            return;
        if (_disposableActions.Count < 1)
        {
            _property.Value = value;
            return;
        }
        _property.Value = RecalculateValue(value);
    }

    private T RecalculateValue(T value)
    {
        T tempValue = value;
        for (int i = _disposableActions.Count; i >= 0; i--)
        {
            tempValue = _disposableActions[i].Invoke(tempValue);
        }

        return tempValue;
    }
    
    public new void Dispose()
    {
        _disposableActions.Clear();
        _property.Value = RecalculateValue(_property.Value);
    }
}

public class EmptyClass
{
    
}