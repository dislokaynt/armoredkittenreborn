﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoystickInputController : MonoBehaviour
{
    public LayerMask BlocksJoysticksLayer;
    public EventSystem GameEventSystem;
    public List<GraphicRaycaster> UiRaycasters;
    public JoystickView CurrentJoystick;
    public JoystickTypeEnum JoystickType;

    private KeyValuePair<int, JoystickDisposer> _joystickEnableStatusPair;

    private RectTransform _rectTransform;
    public static Dictionary<JoystickTypeEnum, Vector2> JoystickValues;
    public static Dictionary<JoystickTypeEnum, OrSubstance> PressedJoysticksStatus;
    
    static JoystickInputController()
    {
        JoystickValues = new Dictionary<JoystickTypeEnum, Vector2>();
        PressedJoysticksStatus = new Dictionary<JoystickTypeEnum, OrSubstance>();
    }
    
    private void Awake()
    {
        if (!JoystickValues.ContainsKey(JoystickType))
        {
            JoystickValues.Add(JoystickType, Vector2.zero);
            PressedJoysticksStatus.Add(JoystickType, new OrSubstance());
        }
        _rectTransform = (RectTransform) transform;
        _joystickEnableStatusPair = new KeyValuePair<int, JoystickDisposer>(-1, null);
    }

    void Update()
    {
        var touches = GetTouches();
        var count = touches.Count;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                var curTouch = touches[i];
                if (curTouch.phase == TouchPhase.Began)
                {
                    if (!CheckInnerBox(curTouch.position))
                        continue;
                    List<RaycastResult> results = new List<RaycastResult>();
                    var pointerEventData = new PointerEventData(GameEventSystem) {position = curTouch.position};
                    //Check layer mask for all canvases in list
                    var foundWrongMask = UiRaycasters.Any(uiRaycaster =>
                    {
                        uiRaycaster.Raycast(pointerEventData, results);
                        //Binary magic for check if 01101100 and 00001000 have something same
                        if (results.Any(hit => (BlocksJoysticksLayer.value & (1 << hit.gameObject.layer)) > 0))
                            return true;
                        return false;
                    });
                    if (foundWrongMask)
                        continue;
                    
                    if (_joystickEnableStatusPair.Key == -1)
                    {
                        _joystickEnableStatusPair = new KeyValuePair<int, JoystickDisposer>(curTouch.fingerId,
                            new JoystickDisposer(() =>
                            {
                                SetActiveSafe(CurrentJoystick, false);
                                PressedJoysticksStatus[JoystickType].Set(false);
                            }));

                        CurrentJoystick.transform.position =
                            new Vector2(curTouch.position.x, curTouch.position.y);
                        SetActiveSafe(CurrentJoystick, true);
                    }
                    PressedJoysticksStatus[JoystickType].Set(true);
                }

                if (curTouch.phase == TouchPhase.Moved && _joystickEnableStatusPair.Key != -1 &&
                    curTouch.fingerId == _joystickEnableStatusPair.Key)
                {
                    JoystickValues[JoystickType] = CurrentJoystick.ChangePosition(curTouch.position);
                }

                if (curTouch.phase == TouchPhase.Ended && _joystickEnableStatusPair.Key != -1 &&
                    curTouch.fingerId == _joystickEnableStatusPair.Key)
                {
                    JoystickValues[JoystickType] = CurrentJoystick.ResetPosition();
                    _joystickEnableStatusPair.Value.Dispose();
                    _joystickEnableStatusPair = new KeyValuePair<int, JoystickDisposer>(-1, null);
                }
            }
        }
    }

    private bool CheckInnerBox(Vector2 touchPosition)
    {
        var newRect = RectTransformToScreenSpace(_rectTransform);
        var xMin = newRect.x;
        var yMin = newRect.y;
        var xMax = newRect.x + newRect.width;
        var yMax = newRect.y + newRect.height;
        return touchPosition.x > xMin && touchPosition.x < xMax && touchPosition.y > yMin && touchPosition.y < yMax;
    }

    private static Rect RectTransformToScreenSpace(RectTransform rectTransform)
    {
        var size = Vector2.Scale(rectTransform.rect.size, rectTransform.lossyScale);
        return new Rect((Vector2) rectTransform.position - size * 0.5f, size);
    }

    private TouchCreator _lastFakeTouch;

    private List<Touch> GetTouches()
    {
        //StackOverflow code for simulate fake touches if you in Editor
        var touches = new List<Touch>();
        touches.AddRange(Input.touches);
#if UNITY_EDITOR
        if (_lastFakeTouch == null) _lastFakeTouch = new TouchCreator();
        if (Input.GetMouseButtonDown(0))
        {
            _lastFakeTouch.phase = TouchPhase.Began;
            _lastFakeTouch.deltaPosition = new Vector2(0, 0);
            _lastFakeTouch.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _lastFakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _lastFakeTouch.phase = TouchPhase.Ended;
            var newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _lastFakeTouch.deltaPosition = newPosition - _lastFakeTouch.position;
            _lastFakeTouch.position = newPosition;
            _lastFakeTouch.fingerId = 0;
        }
        else if (Input.GetMouseButton(0))
        {
            var newPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            _lastFakeTouch.deltaPosition = newPosition - _lastFakeTouch.position;
            _lastFakeTouch.phase = Math.Abs(_lastFakeTouch.deltaPosition.magnitude) < 0.001f
                ? TouchPhase.Stationary
                : TouchPhase.Moved;
            _lastFakeTouch.position = newPosition;
            _lastFakeTouch.fingerId = 0;
        }
        else
        {
            _lastFakeTouch = null;
        }

        if (_lastFakeTouch != null) touches.Add(_lastFakeTouch.Create());
#endif


        return touches;
    }

    private void SetActiveSafe(Component target, bool state)
    {
        //my safe method for exclude double activates of objects
        if (target == null) return;
        if (target.gameObject.activeSelf == state) return;
        target.gameObject.SetActive(state);
    }
}

public class JoystickDisposer : IDisposable
{
    public JoystickTypeEnum JoystickType;
    private readonly Action _actionToDispose;

    public JoystickDisposer(Action disposableAction)
    {
        _actionToDispose = disposableAction;
    }

    public void Dispose()
    {
        if (_actionToDispose != null)
        {
            _actionToDispose.Invoke();
        }
    }
}

public class TouchCreator
{
    static Dictionary<string, FieldInfo> fields;

    object touch;

    public float deltaTime
    {
        get { return ((Touch) touch).deltaTime; }
        set { fields["m_TimeDelta"].SetValue(touch, value); }
    }

    public int tapCount
    {
        get { return ((Touch) touch).tapCount; }
        set { fields["m_TapCount"].SetValue(touch, value); }
    }

    public TouchPhase phase
    {
        get { return ((Touch) touch).phase; }
        set { fields["m_Phase"].SetValue(touch, value); }
    }

    public Vector2 deltaPosition
    {
        get { return ((Touch) touch).deltaPosition; }
        set { fields["m_PositionDelta"].SetValue(touch, value); }
    }

    public int fingerId
    {
        get { return ((Touch) touch).fingerId; }
        set { fields["m_FingerId"].SetValue(touch, value); }
    }

    public Vector2 position
    {
        get { return ((Touch) touch).position; }
        set { fields["m_Position"].SetValue(touch, value); }
    }

    public Vector2 rawPosition
    {
        get { return ((Touch) touch).rawPosition; }
        set { fields["m_RawPosition"].SetValue(touch, value); }
    }

    public Touch Create()
    {
        return (Touch) touch;
    }

    public TouchCreator()
    {
        touch = new Touch();
    }

    static TouchCreator()
    {
        fields = new Dictionary<string, FieldInfo>();
        foreach (var f in typeof(Touch).GetFields(BindingFlags.Instance | BindingFlags.NonPublic))
        {
            fields.Add(f.Name, f);
        }
    }
}

public enum JoystickTypeEnum
{
    Walk,
    Attack
}