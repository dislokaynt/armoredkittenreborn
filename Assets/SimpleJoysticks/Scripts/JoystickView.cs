﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickView : MonoBehaviour
{
    public Transform JoystickHandle;

    public float MaxDistance;

    private Transform _transform;
    private Vector2 _zeroPos;

    private void Awake()
    {
        if (_transform == null)
        {
            _transform = transform;
            //set maxDistance scale from the etalon sizes (you can change them if you will)
            var distanceScaler = Screen.height < Screen.width
                ? (Screen.height / 800f + Screen.width / 1280f)
                : (Screen.width / 800f + Screen.height / 1280f);
            MaxDistance = MaxDistance * (distanceScaler / 2);
        }
    }

    private void OnEnable()
    {
        _zeroPos = transform.position;
    }

    public Vector2 ChangePosition(Vector2 newPosition)
    {
        Vector2 currentPos = newPosition;
        if (Vector2.Distance(_zeroPos, currentPos) > MaxDistance)
        {
            var deltaX = currentPos.x - _zeroPos.x;
            var deltaY = currentPos.y - _zeroPos.y;
            var thetaRadians = Mathf.Atan2(deltaY, deltaX);
            var sin = Mathf.Sin(thetaRadians);
            var cos = Mathf.Cos(thetaRadians);
            JoystickHandle.position = new Vector3(_zeroPos.x + cos * MaxDistance, _zeroPos.y + sin * MaxDistance, 0);
            return new Vector2(cos, sin);
        }

        JoystickHandle.position = new Vector3(currentPos.x, currentPos.y, 0);
        return new Vector2((currentPos.x - _zeroPos.x) / MaxDistance, (currentPos.y - _zeroPos.y) / MaxDistance);
    }

    public Vector2 ResetPosition()
    {
        JoystickHandle.transform.position = _zeroPos;
        return new Vector2(0,0);
    }
}